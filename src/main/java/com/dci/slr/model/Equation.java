package com.dci.slr.model;


import lombok.Data;

import java.util.ArrayList;

@Data
public class Equation {
	private double _eqId;
	private double _a;
	private double _b;
	public ArrayList<Coordinate> _coordinates = new ArrayList<>();
}