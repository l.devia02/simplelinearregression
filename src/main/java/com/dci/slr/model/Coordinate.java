package com.dci.slr.model;

import lombok.Data;

@Data
public class Coordinate {
	private double _corId;
	private double _x;
	private double _y;
}